package decoder

import (
	"errors"
	"strings"

	"gitlab.com/arnef/covcert/internal/hc1"
	"gitlab.com/arnef/covcert/internal/nl2"
	"gitlab.com/arnef/covcert/pkg/covcert"
)

var ErrInvalidValue error = errors.New("invalid code value")

func DecodeString(code string) (covcert.CovCert, error) {
	if strings.HasPrefix(code, "HC1:") {
		return hc1.DecodeString(code)
	}
	if strings.HasPrefix(code, "NL2:") {
		return nl2.DecodeString(code)
	}

	return nil, ErrInvalidValue
}
