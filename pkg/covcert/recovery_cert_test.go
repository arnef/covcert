package covcert_test

import (
	"testing"

	"gitlab.com/arnef/covcert/pkg/covcert"
	"gitlab.com/arnef/covcert/pkg/decoder"
	"gitlab.com/arnef/covcert/tests/assert"
)

func TestDecodeRecovery(t *testing.T) {
	assert := assert.New(t)
	recoveryCode := "HC1:6BFOXN*TS0BI$ZD-PHQ7I9AD66V5B22CH9M9ESI9XBHXK-%69LQOGI.*V76GCV4*XUA2P-FHT-HNTI4L6N$Q%UG/YL WO*Z7ON15 BM0VM.JQ$F4W17PG4.VAS5EG4V*BRL0K-RDY5RWOOH6PO9:TUQJAJG9-*NIRICVELZUZM9EN9-O9:PICIG805CZKHKB-43.E3KD3OAJ6*K6ZCY73JC3KD3ZQTWD3E.KLC8M3LP-89B9K+KB2KK3M*EDZI9$JAQJKKIJX2MM+GWHKSKE MCAOI8%MCU5VTQDPIMQK9*O7%NC.UTWA6QK.-T3-SY$NCU5CIQ 52744E09TBOC.UKMI$8R+1A7CPFRMLNKNM8JI0JPGN:0K7OOBRLY667SYHJL9B7VPO:SWLH1/S4KQQK0$5REQT5RN1FR%SHPLRKWJO8LQ84EBC$-P4A0V1BBR5XWB3OCGEK:$8HHOLQOZUJ*30Q8CD1"
	cert, err := decoder.DecodeString(recoveryCode)
	assert.NoError(err)

	rcert, ok := cert.(covcert.RecoveryCert)
	assert.MustTrue("recovery cert", ok)
	assert.Equals("targetDisease", "COVID-19", rcert.TargetDisease())
	assert.Equals("date first positive test rest", "2021-01-10", rcert.DateOfFirstPositiveTest())
	assert.Equals("member state of test", "Germany", rcert.MemberStateOfTest())
	assert.Equals("valid from", "2021-05-29", rcert.ValidFrom().Format("2006-01-02"))
	assert.Equals("unique certifiace identifier", "01DE/5CWLU12RNOB9RXSEOP6FG8#W", rcert.UniqueIdentifier())
}
