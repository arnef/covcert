package covcert

import "time"

type VaccinationCert interface {
	TargetDisease() string
	Vaccine() string
	VaccineType() string
	Manufacturer() string
	Vaccinations() (int, int)
	DateOfVaccination() string
	MemberStateOfVaccination() string
	UniqueIdentifier() string
	IsComplete() bool
	DaysSinceVaccination(now time.Time) int
	HasFullProtection(now time.Time) bool
}
