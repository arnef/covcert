package covcert

import "time"

type RecoveryCert interface {
	TargetDisease() string
	MemberStateOfTest() string
	DateOfFirstPositiveTest() string
	ValidFrom() time.Time
	UniqueIdentifier() string
}
