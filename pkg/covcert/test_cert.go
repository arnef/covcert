package covcert

import "time"

type TestCert interface {
	TargetDisease() string
	IsNegative() bool
	TypeOfTest() string
	DateTimeOfSampleCollection() time.Time
	TestResult() string
	MemberStateOfTest() string
	UniqueIdentifier() string
	TestingCentre() string
}
