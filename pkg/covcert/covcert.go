package covcert

import (
	"time"
)

type CovCert interface {
	ValidUntil() time.Time
	Issuer() string
	Holder() Holder
	String() string
}

type Holder struct {
	FamilyName  string
	GivenName   string
	DateOfBirth string
}
