package covcert_test

import (
	"testing"
	"time"

	"gitlab.com/arnef/covcert/pkg/covcert"
	"gitlab.com/arnef/covcert/pkg/decoder"
	"gitlab.com/arnef/covcert/tests/assert"
)

func TestDecodeCompleteVaccination(t *testing.T) {
	assert := assert.New(t)
	vaccinationCode := "HC1:6BF+70790T9WJWG.FKY*4GO0.O1CV2 O5 N2FBBRW1*70HS8WY04AC*WIFN0AHCD8KD97TK0F90KECTHGWJC0FDC:5AIA%G7X+AQB9746HS80:54IBQF60R6$A80X6S1BTYACG6M+9XG8KIAWNA91AY%67092L4WJCT3EHS8XJC$+DXJCCWENF6OF63W5NW6WF6%JC QE/IAYJC5LEW34U3ET7DXC9 QE-ED8%E.JCBECB1A-:8$96646AL60A60S6Q$D.UDRYA 96NF6L/5QW6307KQEPD09WEQDD+Q6TW6FA7C466KCN9E%961A6DL6FA7D46JPCT3E5JDLA7$Q6E464W5TG6..DX%DZJC6/DTZ9 QE5$CB$DA/D JC1/D3Z8WED1ECW.CCWE.Y92OAGY8MY9L+9MPCG/D5 C5IA5N9$PC5$CUZCY$5Y$527B+A4KZNQG5TKOWWD9FL%I8U$F7O2IBM85CWOC%LEZU4R/BXHDAHN 11$CA5MRI:AONFN7091K9FKIGIY%VWSSSU9%01FO2*FTPQ3C3F"
	cert, err := decoder.DecodeString(vaccinationCode)
	assert.NoError(err)

	vcert, ok := cert.(covcert.VaccinationCert)
	if !ok {
		t.Fatalf("expeceted cert to be vacination cert")
	}
	assert.Equals("targetDisease", "COVID-19", vcert.TargetDisease())
	assert.Equals("vaccine", "COVID-19 Vaccine Moderna", vcert.Vaccine())
	assert.Equals("vaccine type", "SARS-CoV-2 mRNA vaccine", vcert.VaccineType())
	assert.Equals("manufacturer", "Moderna Biotech Spain S.L.", vcert.Manufacturer())

	if doses, total := vcert.Vaccinations(); doses != 2 || total != 2 {
		t.Errorf(`expeceted vaccinations to be "%d/%d" but got "%d/%d`, 2, 2, doses, total)
	}
	assert.Equals("date of vaccination", "2021-05-29", vcert.DateOfVaccination())
	assert.Equals("member state of vaccination", "Germany", vcert.MemberStateOfVaccination())
	assert.Equals("unique certifiace identifier", "01DE/IZ12345A/5CWLU12RNOB9RXSEOP6FG8#W", vcert.UniqueIdentifier())

	now, _ := time.Parse("2006-01-02", "2021-06-12")
	assert.Equals("days since vaccincation", 14, vcert.DaysSinceVaccination(now))
	assert.Equals("is complete", true, vcert.IsComplete())
	assert.Equals("has full protection", false, vcert.HasFullProtection(now))
	tomorrow := now.AddDate(0, 0, 1)
	assert.Equals("has full protection", true, vcert.HasFullProtection(tomorrow))
}

func TestDecodeInCompleteVaccination(t *testing.T) {
	assert := assert.New(t)
	vaccinationCode := "HC1:6BF+70790T9WJWG.FKY*4GO0.O1CV2 O5 N2FBBRW1*70HS8WY04AC*WIFN0AHCD8KD97TK0F90KECTHGWJC0FDC:5AIA%G7X+AQB9746HS80:54IBQF60R6$A80X6S1BTYACG6M+9XG8KIAWNA91AY%67092L4WJCT3EHS8XJC +DXJCCWENF6OF63W5NW6WF6%JC QE/IAYJC5LEW34U3ET7DXC9 QE-ED8%E.JCBECB1A-:8$96646AL60A60S6Q$D.UDRYA 96NF6L/5QW6307KQEPD09WEQDD+Q6TW6FA7C466KCN9E%961A6DL6FA7D46JPCT3E5JDLA7$Q6E464W5TG6..DX%DZJC6/DTZ9 QE5$CB$DA/D JC1/D3Z8WED1ECW.CCWE.Y92OAGY8MY9L+9MPCG/D5 C5IA5N9$PC5$CUZCY$5Y$527BHB6*L8ARHDJL.Q7*2T7:SCNFZN70H6*AS6+T$D9UCAD97R8NIBO+/RJVE$9PAGPTBIZEP MO-Q0:R13IURRQ5MV93M9V3X2U:NDZSF"
	cert, err := decoder.DecodeString(vaccinationCode)
	assert.NoError(err)

	vcert, ok := cert.(covcert.VaccinationCert)
	assert.MustTrue("vaccination cert", ok)
	assert.Equals("targetDisease", "COVID-19", vcert.TargetDisease())
	assert.Equals("vaccine", "COVID-19 Vaccine Moderna", vcert.Vaccine())
	assert.Equals("vaccine type", "SARS-CoV-2 mRNA vaccine", vcert.VaccineType())
	assert.Equals("manufacturer", "Moderna Biotech Spain S.L.", vcert.Manufacturer())

	if doses, total := vcert.Vaccinations(); doses != 1 || total != 2 {
		t.Errorf(`expeceted vaccinations to be "%d/%d" but got "%d/%d`, 1, 2, doses, total)
	}
	assert.Equals("date of vaccination", "2021-05-29", vcert.DateOfVaccination())
	assert.Equals("member state of vaccination", "Germany", vcert.MemberStateOfVaccination())
	assert.Equals("unique certifiace identifier", "01DE/IZ12345A/5CWLU12RNOB9RXSEOP6FG8#W", vcert.UniqueIdentifier())

	now, _ := time.Parse("2006-01-02", "2021-06-12")
	assert.Equals("days since vaccincation", 14, vcert.DaysSinceVaccination(now))
	assert.Equals("is complete", false, vcert.IsComplete())
	assert.Equals("has full protection", false, vcert.HasFullProtection(now))
	tomorrow := now.AddDate(0, 0, 1)
	assert.Equals("has full protection", false, vcert.HasFullProtection(tomorrow))
}
