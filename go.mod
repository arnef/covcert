module gitlab.com/arnef/covcert

go 1.16

require (
	github.com/fxamacker/cbor/v2 v2.3.0
	github.com/minvws/base45-go v0.1.0
	github.com/pariz/gountries v0.0.0-20211104183308-60c653385099
	github.com/stretchr/testify v1.7.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
