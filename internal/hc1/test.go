package hc1

import (
	"strings"
	"time"

	"gitlab.com/arnef/covcert/internal/euvaluerepo"
	"gitlab.com/arnef/covcert/pkg/covcert"
)

type Test struct {
	TargetDisease     string    `cbor:"tg"`
	TestType          string    `cbor:"tt"`
	TestName          string    `cbor:"nm"`
	Manufacturer      string    `cbor:"ma"`
	SampleCollection  time.Time `cbor:"sc"`
	TestResult        string    `cbor:"tr"`
	TestCentre        string    `cbor:"tc"`
	Country           string    `cbor:"co"`
	CertificateIssuer string    `cbor:"is"`
	ID                string    `cbor:"ci"`
}

type TestCert struct {
	cert *CovCert
	data string
}

func (t *TestCert) Issuer() string {
	return t.cert.Tests[0].CertificateIssuer
}
func (t *TestCert) Holder() covcert.Holder {
	return covcert.Holder{
		FamilyName:  t.cert.Name.FamilyName,
		GivenName:   t.cert.Name.GivenName,
		DateOfBirth: t.cert.BirthDateFormatted(),
	}
}
func (t *TestCert) ValidUntil() time.Time {
	collection := t.DateTimeOfSampleCollection()
	if t.IsNegative() {
		switch t.cert.Tests[0].TestType {
		case "LP217198-3": // Antigen Test
			return collection.AddDate(0, 0, 1)
		case "LP6464-4": // PCR Test
			return collection.AddDate(0, 0, 2)
		}
	}
	// positive tests can be ignored
	return collection
}
func (t *TestCert) String() string {
	return t.data
}

//
func (t *TestCert) TargetDisease() string {
	return euvaluerepo.GetDiseaseAgentName(t.cert.Tests[0].TargetDisease)
}
func (t *TestCert) IsNegative() bool {
	// positive result would be 260373001
	return t.cert.Tests[0].TestResult == "260415000"
}

func (t *TestCert) TypeOfTest() string {
	return euvaluerepo.GetTestTypeName(t.cert.Tests[0].TestType)
}

func (t *TestCert) DateTimeOfSampleCollection() time.Time {
	return t.cert.Tests[0].SampleCollection
}

func (t *TestCert) TestResult() string {
	return euvaluerepo.GetTestResultName(t.cert.Tests[0].TestResult)
}
func (t *TestCert) MemberStateOfTest() string {
	return euvaluerepo.GetCountryName(t.cert.Tests[0].Country)
}
func (t *TestCert) UniqueIdentifier() string {
	return strings.TrimPrefix(t.cert.Tests[0].ID, "URN:UVCI:")
}
func (t *TestCert) TestingCentre() string {
	return t.cert.Tests[0].TestCentre
}
