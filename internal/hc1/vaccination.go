package hc1

import (
	"strings"
	"time"

	"gitlab.com/arnef/covcert/internal/euvaluerepo"
	"gitlab.com/arnef/covcert/pkg/covcert"
)

type Vaccination struct {
	TargetDisease     string `cbor:"tg"`
	VaccineCode       string `cbor:"vp"`
	Product           string `cbor:"mp"`
	Manufacturer      string `cbor:"ma"`
	Occurence         string `cbor:"dt"`
	Country           string `cbor:"co"`
	CertificateIssuer string `cbor:"is"`
	ID                string `json:"ci"`
	TotalSerialDoses  int    `cbor:"sd"`
	DoseNumber        int    `cbor:"dn"`
}

type VaccinationCert struct {
	cert *CovCert
	data string
}

func (c *VaccinationCert) Issuer() string {
	return c.cert.Vaccinations[0].CertificateIssuer
}
func (c *VaccinationCert) Holder() covcert.Holder {
	return covcert.Holder{
		FamilyName:  c.cert.Name.FamilyName,
		GivenName:   c.cert.Name.GivenName,
		DateOfBirth: c.cert.BirthDateFormatted(),
	}
}

// just for sorting certificates vaccination cert has only a technical valid until date.
func (c *VaccinationCert) ValidUntil() time.Time {
	validUntil := time.Now() // fallback
	if occ, err := ParseDay(c.DateOfVaccination()); err == nil {

		if c.HasFullProtection(validUntil) {
			validUntil = occ.AddDate(0, 0, 15)
		} else {
			validUntil = occ.AddDate(0, 0, 14)
		}
	}

	return validUntil
}
func (c *VaccinationCert) String() string {
	return c.data
}
func (c *VaccinationCert) TargetDisease() string {
	return euvaluerepo.GetDiseaseAgentName(c.cert.Vaccinations[0].TargetDisease)
}
func (c *VaccinationCert) Vaccine() string {
	return euvaluerepo.GetProductName(c.cert.Vaccinations[0].Product)
}
func (c *VaccinationCert) VaccineType() string {
	return euvaluerepo.GetProphylaxisName(c.cert.Vaccinations[0].VaccineCode)
}
func (c *VaccinationCert) Manufacturer() string {
	return euvaluerepo.GetManufacturerName(c.cert.Vaccinations[0].Manufacturer)
}
func (c *VaccinationCert) Vaccinations() (int, int) {
	v := c.cert.Vaccinations[0]
	return v.DoseNumber, v.TotalSerialDoses
}
func (c *VaccinationCert) DateOfVaccination() string {
	return c.cert.Vaccinations[0].Occurence
}
func (c *VaccinationCert) MemberStateOfVaccination() string {
	return euvaluerepo.GetCountryName(c.cert.Vaccinations[0].Country)
}
func (c *VaccinationCert) UniqueIdentifier() string {
	return strings.TrimPrefix(c.cert.Vaccinations[0].ID, "URN:UVCI:")
}
func (c *VaccinationCert) IsComplete() bool {
	doses, total := c.Vaccinations()
	return doses >= total
}
func (c *VaccinationCert) DaysSinceVaccination(now time.Time) int {
	vaccinationDay, err := time.Parse("2006-01-02", c.DateOfVaccination())
	// ignore time for calculation
	now, _ = time.Parse("2006-01-02", now.Format("2006-01-02"))
	if err != nil {
		return -1
	}
	return int(now.Sub(vaccinationDay).Hours()) / 24
}
func (c *VaccinationCert) isBooster() bool {
	doses, total := c.Vaccinations()

	isComplete := c.IsComplete()
	product := c.cert.Vaccinations[0].Product
	return (isComplete && doses > 2) || (doses > total) || (product == Janssen && doses == 2)
}
func (c *VaccinationCert) HasFullProtection(now time.Time) bool {

	return c.IsComplete() && (c.DaysSinceVaccination(now) > 14 || c.isBooster())
}

const (
	Biontach    = "EU/1/20/1528"
	Moderna     = "EU/1/20/1507"
	Astrazeneca = "EU/1/21/1529"
	Janssen     = "EU/1/20/1525"
)
