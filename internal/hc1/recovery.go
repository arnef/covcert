package hc1

import (
	"strings"
	"time"

	"gitlab.com/arnef/covcert/internal/euvaluerepo"
	"gitlab.com/arnef/covcert/pkg/covcert"
)

type Recovery struct {
	TargetDisease     string `cbor:"tg"`
	FirstResult       string `cbor:"fr"`
	ValidFrom         string `cbor:"df"`
	ValidUntil        string `cbor:"du"`
	Country           string `cbor:"co"`
	CertificateIssuer string `cbor:"is"`
	ID                string `cbor:"ci"`
}

type RecoveryCert struct {
	cert *CovCert
	data string
}

func (r *RecoveryCert) Issuer() string {
	return r.cert.Recoveries[0].CertificateIssuer
}
func (r *RecoveryCert) Holder() covcert.Holder {
	return covcert.Holder{
		FamilyName:  r.cert.Name.FamilyName,
		GivenName:   r.cert.Name.GivenName,
		DateOfBirth: r.cert.BirthDateFormatted(),
	}
}
func (r *RecoveryCert) ValidUntil() time.Time {
	t, _ := time.Parse("2006-01-02", r.cert.Recoveries[0].ValidUntil)
	return t
}
func (r *RecoveryCert) String() string {
	return r.data
}

//

func (r *RecoveryCert) TargetDisease() string {
	return euvaluerepo.GetDiseaseAgentName(r.cert.Recoveries[0].TargetDisease)
}

func (r *RecoveryCert) MemberStateOfTest() string {
	return euvaluerepo.GetCountryName(r.cert.Recoveries[0].Country)
}

func (r *RecoveryCert) DateOfFirstPositiveTest() string {
	return r.cert.Recoveries[0].FirstResult
}

func (r *RecoveryCert) ValidFrom() time.Time {
	t, _ := time.Parse("2006-01-02", r.cert.Recoveries[0].ValidFrom)
	return t
}

func (r *RecoveryCert) UniqueIdentifier() string {
	return strings.TrimPrefix(r.cert.Recoveries[0].ID, "URN:UVCI:")
}
