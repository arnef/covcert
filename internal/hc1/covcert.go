package hc1

import (
	"fmt"
	"time"
)

type CovCert struct {
	Issuer       string        `cbor:"-"`
	ValidFrom    time.Time     `cbor:"-"`
	ValidUntil   time.Time     `cbor:"-"`
	Version      string        `cbor:"ver"`
	Name         Name          `cbor:"nam"`
	DateOfBirth  string        `cbor:"dob"`
	Vaccinations []Vaccination `cbor:"v"`
	Recoveries   []Recovery    `cbor:"r"`
	Tests        []Test        `cbor:"t"`
}

func (c *CovCert) BirthDateFormatted() string {
	switch len(c.DateOfBirth) {
	case 0: // empty
		return "XXXX-XX-XX"
	case 4: // "2021"
		return fmt.Sprintf("%s-XX-XX", c.DateOfBirth)
	case 7: // "2021-01"
		return fmt.Sprintf("%s-XX", c.DateOfBirth)
	default:
		return c.DateOfBirth
	}
}

type Name struct {
	FamilyName    string `cbor:"fn"`
	FamilyNameStd string `cbor:"fnt"`
	GivenName     string `cbor:"gn"`
	GivenNameStd  string `cbor:"gnt"`
}
