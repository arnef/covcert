package nl2

import (
	"encoding/asn1"
	"errors"
	"strconv"
	"strings"

	"math/big"

	"github.com/minvws/base45-go/base45"
	"gitlab.com/arnef/covcert/pkg/covcert"
)

func DecodeString(val string) (covcert.CovCert, error) {
	unprefixed, err := unprefix(val)

	if err != nil {
		return nil, err
	}
	decoded, err := base45.Base45Decode([]byte(unprefixed))
	if err != nil {
		return nil, err
	}
	vals := ProofSerializationV2{}
	_, err = asn1.Unmarshal(decoded, &vals)
	if err != nil {
		return nil, err
	}
	_, issuer, attributes, err := decodeMetadataAttribute(vals.ADisclosed[0])
	if err != nil {
		return nil, err
	}
	data := map[string]string{}
	for i, attr := range attributes {
		val := decodeAttributeInt(vals.ADisclosed[i+1])
		data[attr] = string(val)
	}

	validFrom, err := strconv.Atoi(data["validFrom"])
	if err != nil {
		return nil, err
	}
	validForHours, err := strconv.Atoi(data["validForHours"])
	if err != nil {
		return nil, err
	}
	return &Cert{
		Raw:             val,
		IssuerPkId:      issuer,
		IsSpecimen:      data["isSpecimen"] == "1",
		IsPaperProof:    data["isPaperProof"] == "1",
		ValidFrom:       validFrom,
		ValidForHours:   validForHours,
		FirstNameInital: data["firstNameInitial"],
		LastNameInitial: data["lastNameInitial"],
		BirthDay:        data["birthDay"],
		BirthMonth:      data["birthMonth"],
		Category:        data["category"],
	}, nil
}

type ProofSerializationV2 struct {
	DisclosureTimeSeconds int64
	C                     *big.Int
	A                     *big.Int
	EResponse             *big.Int
	VResponse             *big.Int
	AResponse             *big.Int
	ADisclosed            []*big.Int
}

func unprefix(val string) (string, error) {
	if !strings.HasPrefix(val, "NL2:") {
		return "", errors.New("data does not start with NL2: prefix")
	}

	return strings.TrimPrefix(val, "NL2:"), nil
}

func decodeAttributeInt(a *big.Int) []byte {
	attributeInt := new(big.Int).Set(a)

	// The last bit distinguishes empty vs. optional attributes
	if attributeInt.Bit(0) == 0 {
		// Optional attribute
		return []byte{}
	} else {
		// Empty attribute, right shift to get the actual value
		attributeInt.Rsh(attributeInt, 1)
		return attributeInt.Bytes()
	}
}

func decodeMetadataAttribute(metadataAttribute *big.Int) (credentialVersion int, issuerPkId string, attributeTypes []string, err error) {
	credentialMetadata := &CredentialMetadataSerialization{}

	attributeBytes := decodeAttributeInt(metadataAttribute)
	_, err = asn1.Unmarshal(attributeBytes, credentialMetadata)
	if err != nil {
		return 0, "", nil, errors.New("could not unmarshal metadata attribute")
	}

	// The credential version is defined to always be a single byte
	credentialVersion = int(credentialMetadata.CredentialVersion[0])

	// Determine attribute types for the credential version
	if credentialVersion == 2 {
		attributeTypes = AttributeTypesV2
	} else if credentialVersion == 3 {
		attributeTypes = AttributeTypesV3
	} else {
		return 0, "", nil, errors.New("unrecognized credential version")
	}

	return credentialVersion, credentialMetadata.IssuerPkId, attributeTypes, nil
}

var CredentialVersion = 2
var CredentialVersionBytes = []byte{byte(CredentialVersion)}

var ProofVersionByte byte = '2'

var AttributeTypesV2 = []string{
	"isSpecimen",
	"isPaperProof",
	"validFrom",
	"validForHours",
	"firstNameInitial",
	"lastNameInitial",
	"birthDay",
	"birthMonth",
}

var AttributeTypesV3 = []string{
	"isSpecimen",
	"isPaperProof",
	"validFrom",
	"validForHours",
	"firstNameInitial",
	"lastNameInitial",
	"birthDay",
	"birthMonth",
	"category",
}

type CredentialMetadataSerialization struct {
	// CredentialVersion identifies the credential version, and is always a single byte
	CredentialVersion []byte

	// IssuerPkId identifies the public key to use for verification
	IssuerPkId string
}
