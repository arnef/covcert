package nl2

import (
	"fmt"
	"time"

	"gitlab.com/arnef/covcert/pkg/covcert"
)

type Cert struct {
	IsSpecimen      bool
	IsPaperProof    bool
	ValidFrom       int
	ValidForHours   int
	FirstNameInital string
	LastNameInitial string
	BirthDay        string
	BirthMonth      string
	Category        string
	IssuerPkId      string
	Raw             string
}

// func (c *Cert) Type() covcert.CertType {
// 	return covcert.CertTypeBase
// }
func (c *Cert) Issuer() string {
	return c.IssuerPkId
}

func (c *Cert) ValidUntil() time.Time {
	t := time.Unix(int64(c.ValidFrom), 0)
	return t.Add(time.Duration(c.ValidForHours) * time.Hour)
}

func (c *Cert) Holder() covcert.Holder {

	return covcert.Holder{
		FamilyName:  parseName(c.LastNameInitial),
		GivenName:   parseName(c.FirstNameInital),
		DateOfBirth: fmt.Sprintf("XXXX-%s-%s", parseBirth(c.BirthMonth), parseBirth(c.BirthDay)),
	}
}

func (c *Cert) String() string {
	return c.Raw
}

func parseBirth(val string) string {
	if len(val) == 0 {
		return "XX"
	}
	if len(val) == 1 {
		return "0" + val
	}
	return val
}
func parseName(val string) string {
	if len(val) == 1 {
		return val + "."
	}
	return val
}
