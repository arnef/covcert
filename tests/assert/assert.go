package assert

import "testing"

type Assert struct {
	t *testing.T
}

func New(t *testing.T) *Assert {
	return &Assert{t}
}
func (a *Assert) Equals(desc string, expected, actual interface{}) {
	if actual != expected {
		a.t.Errorf(`expected %s to be "%v" but got "%v"`, desc, expected, actual)
	}
}
func (a *Assert) NoError(err error) {
	if err != nil {
		a.t.Fatalf("expected error to be nil but got %v", err)
	}
}

func (a *Assert) MustTrue(desc string, val bool) {
	if !val {
		a.t.Fatalf("expected %s to be true", desc)
	}
}
